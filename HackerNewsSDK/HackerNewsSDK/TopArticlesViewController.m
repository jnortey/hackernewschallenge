//
//  TopArticlesViewController.m
//  HackerNewsSDK
//
//  Created by Nortey on 1/26/17.
//  Copyright © 2017 Nortey. All rights reserved.
//

#import "TopArticlesViewController.h"
#import "MemoryStore.h"
#import "ArticleTableCell.h"
#import "WebDisplayViewController.h"
#import "Utility.h"
#import "Constants.h"
#import "TopArticlesView.h"

@interface TopArticlesViewController () {
    TopArticlesView* articlesView;
    NSArray* articleArray;
}

@end

@implementation TopArticlesViewController

-(void)loadView {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    self.view = [[TopArticlesView alloc] initWithFrame:CGRectMake(0, 0, screenWidth,screenHeight)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    articlesView = (TopArticlesView*)self.view;
    articlesView.tableView.delegate = self;
    articlesView.tableView.dataSource = self;
    [articlesView.backButton addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
    
    // If the articles have already been precached, we can go ahead and show what is stored locally. Otherwise,
    // show the indicator while we make the request for each article
    articleArray = [[MemoryStore sharedInstance] getTopArticles:HN_NUM_ARTICLES];
    if (articleArray.count == 0) {
        [articlesView.indicatorView startAnimating];
    } else {
        articlesView.indicatorView.hidden = true;
    }
    
    // Even if the articles have been precached, make the request to update the locally stored
    // articles so that the list view will always be up to date and the articles will be in the correct order.
    // Note: In the case that the articles have been precached, this will only make a single request to retrieve
    // the list of top articles.
    [[NSNotificationCenter defaultCenter] postNotificationName:HN_UPDATE_ARTICLES_NOTIFICATION object:self userInfo:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    // Listen for when the articles finish updating to allow the list to dynamically update if there are changes
    [[NSNotificationCenter defaultCenter]
        addObserver:self selector:@selector(updateTable) name:HN_UPDATE_COMPLETE_NOTIFICATION object:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)updateTable {
    [Utility runOnMainThread:^{
        [articlesView.indicatorView stopAnimating];
        articlesView.indicatorView.hidden = true;
        
        articleArray = [[MemoryStore sharedInstance] getTopArticles:HN_NUM_ARTICLES];
        [articlesView.tableView reloadData];
    }];
}

-(IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return articleArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    static NSString *identifier = @"ArticleTableCell";
    ArticleTableCell *cell = (ArticleTableCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if(!cell){
        cell = [[ArticleTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    Article* article = [articleArray objectAtIndex:indexPath.row];
    [cell.titleLabel setText:article.title];
    [cell.articleNumber setText:[NSString stringWithFormat:@"%i", (unsigned int)indexPath.row+1]];
    [cell.metadataLabel setText:[NSString stringWithFormat:@"%ld points by %@ | %ld comments", article.score, article.submittedBy, article.numComments]];
    
    NSString *dateString = [NSDateFormatter localizedStringFromDate:article.time
                                                          dateStyle:NSDateFormatterShortStyle
                                                          timeStyle:NSDateFormatterShortStyle];
    
    [cell.timeLabel setText:[NSString stringWithFormat:@"submitted at: %@", dateString]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    WebDisplayViewController* webViewController = [[WebDisplayViewController alloc] init];
    
    Article* article = [articleArray objectAtIndex:indexPath.row];
    if (article.url != nil) {
        [webViewController setURL:article.url];
    } else {
        // Some articles (such as AskHN items) will not contain a url. In that case, just show the hacker news
        // comment page
        [webViewController setURL:[NSString stringWithFormat:@"https://news.ycombinator.com/item?id=%ld", article.articleId]];
    }
    
    [self presentViewController:webViewController animated:YES completion:^{}];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 85;
}

@end
