//
//  HackerNews.m
//  HackerNews
//
//  Created by Nortey on 1/26/17.
//  Copyright © 2017 Nortey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HackerNews.h"
#import "Utility.h"
#import "TopArticlesViewController.h"
#import "MemoryStore.h"
#import "Constants.h"
#import "Article.h"

@interface HackerNews() {
    BOOL updateInProgress;
}

@end

@implementation HackerNews

+(instancetype) sharedInstance{
    static id _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}

-(id)init {
    self = [super init];
    if(self) {
        updateInProgress = NO;
        
        // Register observer to allow updating the articles locally stored without needing to directly
        // reference this class
        [[NSNotificationCenter defaultCenter]
            addObserver:self selector:@selector(updateArticles) name:HN_UPDATE_ARTICLES_NOTIFICATION object:nil];
    }
    return self;
}

/*
    Can be called to precache hacker news article items
 */
-(void)initSDK {
    [self updateArticles];
}

/*
    Displays the top articles list view. A call to initSDK is not required before calling this method,
    however if initSDK is not called, the list items will be requested after the view controller has
    been presented.
 */
-(void)showHackerNews {
    TopArticlesViewController* viewController = [[TopArticlesViewController alloc] init];
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:viewController animated:YES completion:nil];
}

-(void)updateArticles {
    
    // Prevent multiple concurrent requests to update the locally stored articles
    if (updateInProgress) {
        return;
    }
    updateInProgress = true;
    
    // Fetch the array of top articles
    NSString *dataUrl = @"https://hacker-news.firebaseio.com/v0/topstories.json";
    
    [Utility getJSONDataFromURL:dataUrl withCallback:^(id jsonObject) {
        NSArray* topArticleArray = (NSArray*)jsonObject;
        if (topArticleArray != nil) {
            [self fetchAndStoreArticles:topArticleArray];
        } else {
            // The network request failed (no internet connection, invalid json returned from the request, etc)
            updateInProgress = false;
        }
    }];
}

/*
    The HackerNews API does not support bulk requests for news articles. This method will
    make a separate request for each news article we need to display in the list view.
 */
-(void)fetchAndStoreArticles:(NSArray*)articleIds {
    dispatch_group_t group = dispatch_group_create();
    [[MemoryStore sharedInstance] resetArticleOrder];
    
    for (int i=0; i<HN_NUM_ARTICLES; i++) {
        dispatch_group_enter(group);
        
        // Check if we already have the article in local storage
        long articleId = [[articleIds objectAtIndex:i] longValue];
        Article* article = [[MemoryStore sharedInstance] getArticle:articleId];
        
        // If the article has not already been stored locally, make a network request to retrieve it
        if (article == nil) {
            NSString *dataUrl = [NSString stringWithFormat:@"https://hacker-news.firebaseio.com/v0/item/%ld.json", articleId];
            
            [Utility getJSONDataFromURL:dataUrl withCallback:^(id jsonObject) {
                NSDictionary* articleDict = (NSDictionary*)jsonObject;
                if (articleDict != nil) {
                    [[MemoryStore sharedInstance] addArticle:articleDict withOrder:i];
                }
                dispatch_group_leave(group);
            }];
        } else {
            // If it has been stored locally, the order in which it must appear in the list may
            // have changed
            article.order = i;
            dispatch_group_leave(group);
        }
    }
    
    // A differente request will be made for each article. The dispatch_group will allow us to wait until
    // all of them are complete before sending out the notification that the local articles have
    // been updated
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:HN_UPDATE_COMPLETE_NOTIFICATION object:self userInfo:nil];
        updateInProgress = false;
    });
}

@end
