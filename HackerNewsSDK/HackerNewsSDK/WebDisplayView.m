//
//  WebDisplayView.m
//  HackerNewsSDK
//
//  Created by Nortey on 1/28/17.
//  Copyright © 2017 Nortey. All rights reserved.
//

#import "WebDisplayView.h"

@implementation WebDisplayView

-(id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        CGFloat screenHeight = screenRect.size.height;
        CGFloat headerHeight = 70;
        
        // Webview
        self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, headerHeight, screenWidth,screenHeight-headerHeight)];
        [self addSubview:self.webView];
        
        // Back button
        CGFloat buttonWidth = 80;
        CGFloat buttonCenter = screenWidth/2 - buttonWidth/2;
        
        self.backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.backButton.frame = CGRectMake(buttonCenter, 30, buttonWidth, 25);
        self.backButton.backgroundColor = [UIColor blackColor];
        [self.backButton setTitle:@"Close" forState:UIControlStateNormal];
        [self addSubview:self.backButton];
        
        // Network indicator view
        self.indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        self.indicatorView.center = CGPointMake(CGRectGetMidX(screenRect), CGRectGetMidY(screenRect));
        self.indicatorView.color = [UIColor blackColor];
        [self addSubview:self.indicatorView];
    }
    
    return self;
}

@end
