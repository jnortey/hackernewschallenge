//
//  Article.h
//  HackerNewsSDK
//
//  Created by Nortey on 1/28/17.
//  Copyright © 2017 Nortey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Article : NSObject

@property long order;
@property long articleId;
@property long score;
@property long numComments;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *submittedBy;
@property (nonatomic, strong) NSDate *time;

@end
