//
//  ArticleTableCell.h
//  HackerNewsSDK
//
//  Created by Nortey on 1/27/17.
//  Copyright © 2017 Nortey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleTableCell : UITableViewCell

@property (nonatomic, strong) UILabel *articleNumber;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *metadataLabel;
@property (nonatomic, strong) UILabel *timeLabel;

@end
