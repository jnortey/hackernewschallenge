//
//  MemoryStore.h
//  HackerNewsSDK
//
//  Created by Nortey on 1/28/17.
//  Copyright © 2017 Nortey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Article.h"

@interface MemoryStore : NSObject

+(instancetype) sharedInstance;

-(void)addArticle:(NSDictionary*)articleDict withOrder:(int)order;
-(NSArray*)getTopArticles:(int)limit;
-(Article*)getArticle:(long)articleId;
-(void)resetArticleOrder;

@end
