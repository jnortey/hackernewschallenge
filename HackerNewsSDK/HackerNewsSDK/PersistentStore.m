//
//  PersistentStore.m
//  HackerNewsSDK
//
//  Created by Nortey on 1/26/17.
//  Copyright © 2017 Nortey. All rights reserved.
//

#import "PersistentStore.h"
#import "Utility.h"
#import <CoreData/CoreData.h>

/*
 NOTE: This class is not being used. MemoryStore.h is currently being used to
 cache the article items. This is an example of using core data to locally store
 the articles instead of saving them in memory.
 */
@interface PersistentStore ()

@property (strong) NSManagedObjectContext *managedObjectContext;

@end


@implementation PersistentStore

+(instancetype) sharedInstance{
    static id _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}

-(id)init {
    self = [super init];
    if (self) {
        [self initializeCoreData];
    }
    
    return self;
}

-(void)initializeCoreData {
    NSBundle* bundle = [NSBundle mainBundle];
    NSURL *modelURL = [bundle URLForResource:@"DataModel" withExtension:@"momd"];
    NSManagedObjectModel *mom = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    NSAssert(mom != nil, @"Error initializing Managed Object Model");
    
    NSPersistentStoreCoordinator *psc = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:mom];
    NSManagedObjectContext *moc = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [moc setPersistentStoreCoordinator:psc];
    [self setManagedObjectContext:moc];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *documentsURL = [[fileManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *storeURL = [documentsURL URLByAppendingPathComponent:@"DataModel.sqlite"];
    
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
        NSError *error = nil;
        NSPersistentStoreCoordinator *psc = [[self managedObjectContext] persistentStoreCoordinator];
        [psc addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error];
    });
}

-(void)save {
    [[self managedObjectContext] performBlockAndWait:^{
        NSError *error = nil;
        if ([[self managedObjectContext] save:&error] == NO) {
            NSAssert(NO, @"Error saving context: %@\n%@", [error localizedDescription], [error userInfo]);
        }
    }];
}

-(void)addArticle:(NSDictionary*)articleDict withOrder:(int)order {
    [[self managedObjectContext] performBlockAndWait:^{
//        Article *article =
//            [NSEntityDescription insertNewObjectForEntityForName:@"Article" inManagedObjectContext:[self managedObjectContext]];
//        
//        article.articleId = [[articleDict objectForKey:@"id"] longLongValue];
//        article.title = [articleDict objectForKey:@"title"];
//        article.url = [articleDict objectForKey:@"url"];
//        article.submittedBy = [articleDict objectForKey:@"by"];
//        article.order = order;
//        
//        long time = [[articleDict objectForKey:@"time"] longValue];
//        NSDate *date = [NSDate dateWithTimeIntervalSince1970:time];
//        article.time = date;
    }];
}

-(NSArray*)getTopArticles:(int)limit {
    __block NSArray *results;
    
    [[self managedObjectContext] performBlockAndWait:^{
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Article"];
        [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"order >= 0"]];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"order" ascending:YES];
        [fetchRequest setSortDescriptors:@[sortDescriptor]];
        fetchRequest.fetchLimit = limit;
        
        NSError *error = nil;
        results = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
        if (!results) {
            NSLog(@"Error fetching Articles: %@\n%@", [error localizedDescription], [error userInfo]);
            abort();
        }
    }];
    
    return results;
}

-(Article*)getArticle:(long)articleId {
    __block NSArray *results;
    
    [[self managedObjectContext] performBlockAndWait:^{
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Article"];
        [request setPredicate:[NSPredicate predicateWithFormat:@"articleId == %@", @(articleId)]];
        
        NSError *error = nil;
        results = [[self managedObjectContext] executeFetchRequest:request error:&error];
        if (!results) {
            NSLog(@"Error fetching Articles: %@\n%@", [error localizedDescription], [error userInfo]);
            abort();
        }
    }];
    
    return [results firstObject];
}

-(void)resetArticleOrder {
    [[self managedObjectContext] performBlockAndWait:^{
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Article"];
        
        NSError *error = nil;
        NSArray *results = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
        if (!results) {
            NSLog(@"Error fetching Articles: %@\n%@", [error localizedDescription], [error userInfo]);
            abort();
        }
        
        for(Article* article in results) {
            article.order = -1;
        }
    }];
}

-(void)purgeArticles {
    [[self managedObjectContext] performBlockAndWait:^{
        NSDate *yesterday = [[NSDate date] dateByAddingTimeInterval:-(24*60*60)];
        
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Article"];
        [request setPredicate:[NSPredicate predicateWithFormat:@"dateAdded < %@", yesterday]];
        
        NSError *error = nil;
        NSArray* results = [[self managedObjectContext] executeFetchRequest:request error:&error];
        if (!results) {
            NSLog(@"Error fetching Articles: %@\n%@", [error localizedDescription], [error userInfo]);
            abort();
        }
        
//        for(Article* article in results) {
//            [[self managedObjectContext] deleteObject:article];
//        }
    }];
}

@end
