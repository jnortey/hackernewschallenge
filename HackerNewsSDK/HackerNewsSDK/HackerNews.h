//
//  HackerNews.h
//  HackerNews
//
//  Created by Nortey on 1/26/17.
//  Copyright © 2017 Nortey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HackerNews : NSObject

+(instancetype) sharedInstance;

-(void)initSDK;
-(void)showHackerNews;

@end
