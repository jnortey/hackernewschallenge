//
//  Utility.h
//  HackerNewsSDK
//
//  Created by Nortey on 1/26/17.
//  Copyright © 2017 Nortey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Utility : NSObject

+(void)getJSONDataFromURL: (NSString*)urlString withCallback:(void (^)(id jsonObject))callback;
+(void)runOnMainThread: (void (^)())block;

@end
