//
//  TopArticlesViewController.h
//  HackerNewsSDK
//
//  Created by Nortey on 1/26/17.
//  Copyright © 2017 Nortey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopArticlesViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

-(IBAction)close:(id)sender;

@end
