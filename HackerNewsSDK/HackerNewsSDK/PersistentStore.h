//
//  PersistentStore.h
//  HackerNewsSDK
//
//  Created by Nortey on 1/26/17.
//  Copyright © 2017 Nortey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Article.h"

/*
 NOTE: This class is not being used. MemoryStore.h is currently being used to
 cache the article items. This is an example of using core data to locally store
 the articles instead of saving them in memory.
 */
@interface PersistentStore : NSObject

+(instancetype) sharedInstance;
-(void)addArticle:(NSDictionary*)articleDict withOrder:(int)order;
-(NSArray*)getTopArticles:(int)limit;
-(Article*)getArticle:(long)articleId;
-(void)resetArticleOrder;
-(void)purgeArticles;
-(void)save;

@end
