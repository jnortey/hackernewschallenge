//
//  ArticleTableCell.m
//  HackerNewsSDK
//
//  Created by Nortey on 1/27/17.
//  Copyright © 2017 Nortey. All rights reserved.
//

#import "ArticleTableCell.h"

@implementation ArticleTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        
        _articleNumber = [[UILabel alloc] initWithFrame:CGRectMake(8, 15, 50, 45)];
        _articleNumber.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:24];
        _articleNumber.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_articleNumber];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 0, screenWidth-100, 60)];
        _titleLabel.numberOfLines = 3;
        _titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:13];
        [self addSubview:_titleLabel];
        
        _metadataLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 46, screenWidth-100, 20)];
        _metadataLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:10];
        _metadataLabel.textColor = [UIColor darkGrayColor];
        [self addSubview:_metadataLabel];
        
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 62, screenWidth-100, 20)];
        _timeLabel.font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:10];
        _timeLabel.textColor = [UIColor darkGrayColor];
        [self addSubview:_timeLabel];
    }
    
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
