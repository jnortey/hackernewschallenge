//
//  WebDisplayView.h
//  HackerNewsSDK
//
//  Created by Nortey on 1/28/17.
//  Copyright © 2017 Nortey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebDisplayView : UIView

@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;
@property (nonatomic, strong) UIButton *backButton;

@end
