//
//  Utility.m
//  HackerNewsSDK
//
//  Created by Nortey on 1/26/17.
//  Copyright © 2017 Nortey. All rights reserved.
//

#import "Utility.h"
#import "Constants.h"

@implementation Utility

+(void)getJSONDataFromURL: (NSString*)urlString withCallback:(void (^)(id jsonObject))callback {
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession]
          dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
              if (error) {
                  NSLog(@"ERROR making network request %@", [error description]);
                  callback(nil);
              } else {
                  id parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                  if (error) {
                      NSLog(@"ERROR parsing JSON from network request %@", [error description]);
                      callback(nil);
                  } else {
                      callback(parsedObject);
                  }
              }
          }];
    
    [downloadTask resume];
}

+ (void) runOnMainThread: (void (^)())block {
    if ([NSThread isMainThread]){
        block();
    } else {
        dispatch_sync(dispatch_get_main_queue(), ^{
            block();
        });
    }
}

@end
