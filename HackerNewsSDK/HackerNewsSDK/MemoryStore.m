//
//  LocalStore.m
//  HackerNewsSDK
//
//  Created by Nortey on 1/28/17.
//  Copyright © 2017 Nortey. All rights reserved.
//

#import "MemoryStore.h"

@interface MemoryStore () {
    NSMutableDictionary* articles;
}

@end

@implementation MemoryStore

+(instancetype) sharedInstance{
    static id _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[self alloc] init];
    });
    
    return _sharedInstance;
}

-(id)init {
    self = [super init];
    if (self) {
        articles = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

-(void)addArticle:(NSDictionary*)articleDict withOrder:(int)order {
    Article *article = [[Article alloc] init];
    
    article.articleId = [[articleDict objectForKey:@"id"] longValue];
    article.title = [articleDict objectForKey:@"title"];
    article.url = [articleDict objectForKey:@"url"];
    article.submittedBy = [articleDict objectForKey:@"by"];
    article.score = [[articleDict objectForKey:@"score"] longValue];
    article.numComments = [[articleDict objectForKey:@"descendants"] longValue];
    article.order = order;
    
    long time = [[articleDict objectForKey:@"time"] longValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:time];
    article.time = date;
    
    [articles setObject:article forKey:@(article.articleId)];
}

-(NSArray*)getTopArticles:(int)limit {
    if (articles.count < limit) {
        return @[];
    }
    
    NSArray* articleArray = [articles allValues];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"order" ascending:YES];
    NSArray *sortedArticles = [articleArray sortedArrayUsingDescriptors:@[sortDescriptor]];
    return [sortedArticles subarrayWithRange:NSMakeRange(0, limit)];
}

-(Article*)getArticle:(long)articleId {
    return [articles objectForKey:@(articleId)];
}

/*
    If an item drops out of the top 20, it should not be shown in the list view. Also, if an item
    is already precached but changes position, we want to make sure the position is correctly reflected
    in the list view. 
 
    This method is used to reset the order of all articles so that two articles will not share the same
    order number due to changing positions. (Alternatively we could purge all locally stored articles that
    do not show up in the top 20)
 */
-(void)resetArticleOrder {
    NSArray* articleArray = [articles allValues];
    for(Article* article in articleArray) {
        article.order = 10000;
    }
}

@end
