//
//  WebViewController.m
//  HackerNewsSDK
//
//  Created by Nortey on 1/27/17.
//  Copyright © 2017 Nortey. All rights reserved.
//

#import "WebDisplayViewController.h"
#import "WebDisplayView.h"

@interface WebDisplayViewController () {
    NSString* requestURL;
    WebDisplayView* webDisplayView;
}

@end

@implementation WebDisplayViewController

-(void)loadView {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    self.view = [[WebDisplayView alloc] initWithFrame:CGRectMake(0, 0, screenWidth,screenHeight)];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    webDisplayView = (WebDisplayView*)self.view;
    webDisplayView.webView.delegate = self;
    [webDisplayView.indicatorView startAnimating];
    
    [webDisplayView.backButton addTarget:self action:@selector(close:) forControlEvents:UIControlEventTouchUpInside];
    
    if (requestURL != nil) {
        NSURL *url = [NSURL URLWithString:requestURL];
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
        [webDisplayView.webView loadRequest:urlRequest];
    } else {
        NSLog(@"ERROR - Attempting to show WebViewController witihout first setting URL.");
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [webDisplayView.indicatorView stopAnimating];
    webDisplayView.indicatorView.hidden = TRUE;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)close:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)setURL:(NSString*)url {
    requestURL = url;
}

@end
