//
//  WebViewController.h
//  HackerNewsSDK
//
//  Created by Nortey on 1/27/17.
//  Copyright © 2017 Nortey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebDisplayViewController : UIViewController<UIWebViewDelegate>

-(void)setURL:(NSString*)url;
-(IBAction)close:(id)sender;

@end
