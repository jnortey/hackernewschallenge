//
//  Constants.h
//  HackerNewsSDK
//
//  Created by Nortey on 1/27/17.
//  Copyright © 2017 Nortey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

FOUNDATION_EXPORT int const HN_NUM_ARTICLES;
FOUNDATION_EXPORT NSString* const HN_UPDATE_ARTICLES_NOTIFICATION;
FOUNDATION_EXPORT NSString* const HN_UPDATE_COMPLETE_NOTIFICATION;

@end
