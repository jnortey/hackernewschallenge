//
//  Constants.m
//  HackerNewsSDK
//
//  Created by Nortey on 1/27/17.
//  Copyright © 2017 Nortey. All rights reserved.
//

#import "Constants.h"
#import <Foundation/Foundation.h>

@implementation Constants

int const HN_NUM_ARTICLES = 20;
NSString* const HN_UPDATE_ARTICLES_NOTIFICATION = @"_update";
NSString* const HN_UPDATE_COMPLETE_NOTIFICATION = @"_updateComplete";

@end
