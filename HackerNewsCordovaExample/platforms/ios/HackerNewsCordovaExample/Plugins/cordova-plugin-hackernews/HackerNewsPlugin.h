//
//  HackerNewsPlugin.h
//  HelloCordova
//
//  Created by Nortey on 1/28/17.
//
//

#import <Foundation/Foundation.h>
#import <Cordova/CDV.h>

@interface HackerNewsPlugin : CDVPlugin

- (void)initSDK:(CDVInvokedUrlCommand*)command;
- (void)showHackerNews:(CDVInvokedUrlCommand*)command;

@end
