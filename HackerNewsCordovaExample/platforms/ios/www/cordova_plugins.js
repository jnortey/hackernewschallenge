cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "id": "cordova-plugin-hackernews.device",
        "file": "plugins/cordova-plugin-hackernews/www/hackerNews.js",
        "pluginId": "cordova-plugin-hackernews",
        "clobbers": [
            "hackerNews"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "cordova-plugin-whitelist": "1.3.1",
    "cordova-plugin-hackernews": "0.2.3"
};
// BOTTOM OF METADATA
});