//
//  HackerNewsPlugin.m
//  HelloCordova
//
//  Created by Nortey on 1/28/17.
//
//

#import "HackerNewsPlugin.h"
#import <HackerNewsSDK/HackerNewsSDK.h>

@implementation HackerNewsPlugin

- (void)initSDK:(CDVInvokedUrlCommand*)command {
    [[HackerNews sharedInstance] initSDK];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)showHackerNews:(CDVInvokedUrlCommand*)command {
    [[HackerNews sharedInstance] showHackerNews];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
