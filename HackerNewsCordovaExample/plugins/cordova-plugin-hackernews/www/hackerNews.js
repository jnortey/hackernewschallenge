/*global cordova, module*/

module.exports = {
    initSDK: function (successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "HackerNewsPlugin", "initSDK", []);
    },

    showHackerNews: function (successCallback, errorCallback) {
        cordova.exec(successCallback, errorCallback, "HackerNewsPlugin", "showHackerNews", []);
    }
};
