//
//  ViewController.m
//  HackerNewsExample
//
//  Created by Nortey on 1/26/17.
//  Copyright © 2017 Nortey. All rights reserved.
//

#import "ViewController.h"
#import <HackerNewsSDK/HackerNews.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)initSDK:(id)sender {
    [[HackerNews sharedInstance] initSDK];
}

- (IBAction)showHackerNews:(id)sender {
    [[HackerNews sharedInstance] showHackerNews];
}


@end
