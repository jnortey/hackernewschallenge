//
//  ViewController.h
//  HackerNewsExample
//
//  Created by Nortey on 1/26/17.
//  Copyright © 2017 Nortey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)initSDK:(id)sender;
- (IBAction)showHackerNews:(id)sender;

@end

