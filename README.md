HackerNewsSDK
=========

Static HackerNews Framework for Kiip Mobile Challenge

##How to use the SDK

Copy HackerNewsSDK.framework into your iOS project

	#import <HackerNewsSDK/HackerNews.h>
	
	[[HackerNews sharedInstance] initSDK]; 
	
	[[HackerNews sharedInstance] showHackerNews];

**initSDK** - Precaches HackerNews remote article data

**showHackerNews** - Displays modal list view containing article information. Will fetch remote data if not already cached.

##How to use the Cordova Plugin
Command line:

	cordova plugin add HackerNewsCordovaPlugin

Javascript:

	hackerNews.initSDK(success, failure);
	hackerNews.showHackerNews(success, failure);


##Files
	HackerNewsSDK.framework - Universal static framework containing the SDK
	HackerNewsSDK - Framework SDK xcode project
	HackerNewsCordovaPlugin - Cordova plugin for invoking the SDK
	HackerNewsExample - Example demonstrating use of the SDK
	HackerNewsCordovaExample - Example demonstrating use of the Cordova Plugin

## Additional Notes
1. Since there are several links submitted to Hacker News that use the http protocol, the Transport Security Settings for the application that uses the framework must be modified to allow them. Otherwise, the link will not load when the article is tapped in the 'showHackerNews' table view. These settings have already been modified in the HackerNewsExample project. 
2. When a network request is made, you may see the logs flooded with the dreaded SO_NOAPNFALLBK warnings. [Reference Link](https://forums.developer.apple.com/thread/49777)